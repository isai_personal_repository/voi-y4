from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import UserPassesTestMixin
from django.shortcuts import redirect
from django.http import HttpResponse

class LoginEmailValidationMixin(UserPassesTestMixin):
    def test_func(self):
        if not (self.request.user.email.endswith('@voioutsourcing.com.mx') or \
            self.request.user.email.endswith('@volaris.com')):
            print('PATH: ', '/login/?next=%s' % self.request.path)
            print('Email: ', self.request.user.email)
            # return redirect('/login/?next=%s' % self.request.path)

