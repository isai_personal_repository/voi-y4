import os, sys

import ftplib
from ftplib import FTP
import logging


# Nomenclatura de archivo: 
# Y4_202103_VCHXR_GL_YES_USD_001.csv.verified.zip
class PoolFTPConnection:
    """Class to establish remote connections with remote hosts vía FTP."""
    #def __init__(self, navitaire_path):
    #    self.navitaire_path = navitaire_path

    def connect(self):
        """Method that connects to any remote host"""
        try:
            logging.info('Connecting to current Repository for files extraction . . .')
            sftp = FTP(host=os.getenv('FTP_HOMESERVER_DOMAIN'), timeout=300)
            sftp.set_debuglevel(2)
            sftp.login(user=os.getenv('FTP_HOMESERVER_USR'), passwd=os.getenv('FTP_HOMESERVER_PWD'))
            sftp.set_pasv(False)
            logging.info('Successfully connected!')
            return sftp

        except ftplib.all_errors as e:
            print('FtpLib ERROR: ', e)
            logging.critical('Error trying to connect to the current repository: ', e)
            sys.exit(1)