from django.urls import path

from apps.user.api.v1.views import UserAPI


urlpatterns = [
    path('list/', UserAPI.as_view(), name='list')
]