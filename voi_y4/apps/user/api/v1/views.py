from django.contrib.auth.models import User
from apps.user.api.v1.serializers import UserSerializer

# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework import authentication, permissions
from rest_framework.permissions import IsAdminUser
from rest_framework.generics import ListAPIView

class UserAPI(ListAPIView):
    """
    Retrieve active user list.
    User fields:\n
    1.- date_joined\n
    2.- email\n
    3.- first_name\n
    4.- groups\n
    5.- id\n
    6.- is_active\n
    7.- is_staff\n
    8.- is_superuser\n
    9.- last_login\n
    10.- last_name\n
    11.- logentry\n
    12.- password\n
    13.- user_permissions\n
    14.- username
    """
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer
    permission_classes = [ IsAdminUser ]
