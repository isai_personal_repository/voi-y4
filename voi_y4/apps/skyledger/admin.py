from django.contrib import admin

from apps.skyledger.models import Skyledger

# Register your models here.
admin.site.register(Skyledger)