import os, sys, logging
from datetime import datetime

from ftplib import FTP
from zipfile import ZipFile

import paramiko

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication

from django.contrib.auth.mixins import LoginRequiredMixin

from apps.skyledger.api.v1.serializers import SkyledgerParameterSerializer
# from tools.ftp.pool_connection import PoolFTPConnection


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
now = datetime.now()

# Creating remote host connection instance for work remotely.
# TODO sftp = PoolFTPConnection()
# TODO sftp = sftp.connect()

# Get an instance of a logger
logger = logging.getLogger(__name__)

class SkyledgerAPI(LoginRequiredMixin, APIView):
    """
    - API that process all Q6 / Y4 files obtained from Navitaire.\n
    - Parameters to work:\n
        - Company: Y4 / Q6\n
        - Current year to process.\n
        - Month / month range to process: Jan | Jan - Dec | etc.\n
        - Debit & Credit files to load from the local machine.\n
        - Navitaire *.csv.verified files to process.\n\n
    - It could process just ONE year at time.
    - From this year, the API can process all months, if the user want,
        and if all of them are availables in the current Navitaire files repo to process.
    - This can be configured as a parameters in a form to be submitted before this process begins.
    """
    authentication_classes = [ SessionAuthentication ]
    permission_classes = [ IsAuthenticated ]
    serializer_class = SkyledgerParameterSerializer

    def get(self, request, format=None):
        """
        Return main template of this view.
        """
        context = {}
        context['Message'] = 'Ready to process.'
        return Response(context)

    def post(self, request):
        serializer = SkyledgerParameterSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({ 'serializer': serializer, 'success': False })
        else:
            context = {}
            serializer = serializer.data
            year = serializer['year']
            companies = list(serializer['company'])
            months = self.month_format(list(serializer['month']))

            # 1.- Get filter list for retrieve selected files from remote host.
            # 2.- Move files to a temporal folder in remote host for be extracted.
            navitaire_filepath = os.getenv('FTP_HOMESERVER_NAVITAIRE_FILEPATH')
            self.filter_navitaire_files(
                navitaire_filepath, # FTP repo url
                year,
                months,
                companies
            )
            self.extract_file()

            # self.create_skyledger_df()
            # Self.format_df()
            # self.export()

            #sftp.close()
            #print('FTP Connection closed.\n')

            context['success'] = True
            context['message'] = 'Everything´s OK!'
        return Response(context)

    def month_format(self, month_list):
        months = {
            'Jan': '01',
            'Feb': '02',
            'Mar': '03',
            'Apr': '04',
            'May': '05',
            'Jun': '06',
            'Jul': '07',
            'Ago': '08',
            'Sep': '09',
            'Oct': '10',
            'Nov': '11',
            'Dec': '12'
        }
        formatted_months = []
        for m in month_list:
            formatted_months.append(months[m])
        return formatted_months

    def filter_navitaire_files(
            self,
            navitaire_filepath,
            year,
            months,
            companies
        ):
        """
        Function that get filtered Navitaire files by given arguments,
        like range of months, the current year and the companies, 
        or just one of them.
        """

        # Build filename structure for match in the ftp list retrieving.
        countr = 0
        for c in companies:
            if c == 'Y4':
                companies[countr] = '_GL_'
            if c == 'Q6':
                companies[countr] = '_Q6GL_'
            countr += 1

        logger.debug('Changing remote dir to get filtered filelist.')
        
        # Get filtered filelist from remote host.
        ftp_filelist = []
        sftp.cwd(navitaire_filepath)
        sftp.retrlines('LIST', ftp_filelist.append)
        #print('Closing connection . . . \n')
        #sftp.close()

        # --- Month filter begin ---.
        flag_list = filter_01 = filter_02 = []
        filtered_ftp_filelist = []
        for m in months:
            y_wildcard = '_' + year + m + '_'
            flag_list = [f for f in ftp_filelist if y_wildcard in f]
            filter_01 = filter_01 + flag_list
        # Clean memory of useless list.
        ftp_filelist = []
        for c in companies:
            flag_list = [f for f in filtered_ftp_filelist if c in f]
            filter_02 = filter_02 + flag_list
        # --- Month filter ends ---.

        # Get final filtered filelist to be passed for process all of them only:
        filtered_ftp_filelist = filter_01 + filter_02
        filtered_ftp_filelist = self.get_filename(filtered_ftp_filelist)

        # Connect to remote host to move filtered filelist:
        self.move_files(
            navitaire_filepath, #From path
            os.getenv('FTP_HOMESERVER_TMP_FILEPATH'), # To path
            filtered_ftp_filelist # Filelist to be moved.
        )

    def get_filename(self, ftp_filelist):
        """
        Filename list retrieved to format from FTP, getting a result like this:
        Y4_202103_VCHXR_GL_YES_USD_001.csv.verified.zip.
        As additional, this return the number of filtered items to be processed.
        """
        filelist = []
        for f in ftp_filelist:
            f = f.split(None, 8)
            f = f[-1].lstrip()
            filelist.append(f)
        return filelist
    """
    def download_files(self, filelist):
        #Download uncompressed files from "tmp" folder in remote host.

        sftp = FTP(host=os.getenv('FTP_HOMESERVER_DOMAIN'), timeout=300)
        sftp.set_debuglevel(2)
        sftp.login(user=os.getenv('FTP_HOMESERVER_USR'), passwd=os.getenv('FTP_HOMESERVER_PWD'))
        sftp.set_pasv(False)
        
        zip_local_path = ''
        for f in filelist:
            print('current file: ', f)


            downloaded = open(zip_local_path, 'wb')
            sftp.retrbinary('RETR ' + f, downloaded.write)
            
            #for match in filaname_contains:
            #    sftp.retrlines('LIST *' + match + '*', file_list.append)
    """
    def move_files(self, from_path, to_path, filelist):
        """
        Move selected files to tmp remote folder for be uncompressed.        
        1.- Copiado de archivos *.zip con base a los parametros enviados en folder /tmp/.
        2.- Descomprimido de los archivos alojados en /tmp/.
        3.- Eliminado de los archivos *.zip dentro del folder /tmp/ 
        4.- Copiado de los archivos descomprimidos dentro de /tmp/ a folder en maáquina local. 
        5.- Procesado de archivos recientemente copiados por proceso.
        """

        # Go to selected path in remote host:
        logger.debug('Moving selected filelist in remote host.')
        # TODO sftp.cwd(from_path)
        try:
            # Move selected files from source folder to destination folder:
            for f in filelist:
                print('File to unzip: ', f)
                # TODO sftp.rename(f, to_path + '/' + f)
        except IOError as e:
            logger.error('Error trying to move selected files  to a \
                temporal remote folder. Exiting:\n' + str(e)
            )
            sys.exit(1)

    def extract_file(self):
        """Extract and validate files inside remote /tmp/ folder"""

        navitaire_filepath = os.getenv('FTP_HOMESERVER_NAVITAIRE_FILEPATH')
        tmp_path = os.getenv('FTP_HOMESERVER_TMP_FILEPATH')

        logger.info('Validating *.zip format . . .')
        # TODO sftp.cwd(tmp_path)
        filelist = []
        # TODO sftp.retrlines('LIST', filelist.append)
        filelist = self.get_filename(filelist)

        # Validating zip file processing only and zip file integrity:
        validated_filelist = [
            f if '.zip' in f else None \
            for f in filelist
        ]

        #Extract validated files:
        #logging.info('Unzipping selected files . . .')
        try:
            logger.debug('Connecting vía ssh to a remote host using paramiko.')
            # Extracting in remote /tmp/ File folder:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(
                os.getenv('FTP_HOMESERVER_DOMAIN'),
                username=os.getenv('FTP_HOMESERVER_USR'),
                password=os.getenv('FTP_HOMESERVER_PWD'),
                timeout=5
            )
            logger.debug('Successfully connected (ssh).')

            # Set command to extract zip files inside remote host.
            # Hint: Its necessary to have already installed "7z" package in the remote server:
            # sudo apt install p7zip-full p7zip-rar
            remote_home_path = os.getenv('FTP_HOME_PATH')
            for filename in validated_filelist:
                command = f'7z e ~{tmp_path}/{filename} \
                    -o{remote_home_path}/{tmp_path}/unzipped'
                stdin, stdout, stderr = client.exec_command(command)
                for line in stdout:
                    logger.debug('Extracting file: ', line.strip('\n'))
                    print (line.strip('\n'))

            # Moving back processed zip files to a main navitaire remote folder:
            ftp_filelist = []
            # TODO sftp.cwd(tmp_path)
            # TODO sftp.retrlines('LIST', ftp_filelist.append)
            ftp_filelist = self.get_filename(ftp_filelist)
            ftp_filelist.remove('unzipped')
    

            """Se envía la misma ruta tmp a las 2"""
            self.move_files(
                tmp_path, # From path
                navitaire_filepath, # To path
                ftp_filelist # Filelist to be moved.
            )
            logger.info('/Tmp/ files moved back sucessfully.')
            print('/Tmp/ files moved back sucessfully.')

            client.close()
            logger.info('Paramiko SSH client closed.\n')

        except Exception as e:
            logger.error('Error in file extraction method ' + str(e))
            pass

        #return validated_filelist02


    def create_skyledger_df(self):
        """
        Creates new DataFRame with the given data from files.
        This for return the new DF and follow up the process.
        """
        pass

    def format_df(self):
        """
        Receive the new DF and format the follow columns:
        - PNR: Transaction Key
        - DateFormat: Account Period
        - DateFormat: Reference Date
        - Aggregate column: Local Amount
        """
        pass

    def export(self):
        """
        Export formatted df into a segmented files
        with the respective name and date format.
        """
        pass