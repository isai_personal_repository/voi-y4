from django.urls import path

from apps.skyledger.api.v1.views import SkyledgerAPI

urlpatterns = [
    path('skyledger/', SkyledgerAPI.as_view(), name='cuentas_por_cobrar'),
]