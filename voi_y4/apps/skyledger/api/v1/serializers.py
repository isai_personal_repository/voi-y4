from rest_framework import serializers


class SkyledgerParameterSerializer(serializers.Serializer):

    Y4 = 'Y4'
    Q6 = 'Q6'
    COMPANY = (
        (Y4, 'Y4'),
        (Q6, 'Q6'),
    )

    JAN = 'Jan'
    FEB = 'Feb'
    MAR = 'Mar'
    APR = 'Apr'
    MAY = 'May'
    JUN = 'Jun'
    JUL = 'Jul'
    AGO = 'Ago'
    SEP = 'Sep'
    OCT = 'Oct'
    NOV = 'Nov'
    DIC = 'Dec'
    MONTH=(
        (JAN, 'January'),
        (FEB, 'February'),
        (MAR, 'March'),
        (APR, 'Apr'),
        (MAY, 'May'),
        (JUN, 'June'),
        (JUL, 'July'),
        (AGO, 'August'),
        (SEP, 'September'),
        (OCT, 'October'),
        (NOV, 'November'),
        (DIC, 'December'),
    )
    year = serializers.CharField(max_length=4)
    company = serializers.MultipleChoiceField(choices=COMPANY)
    month = serializers.MultipleChoiceField(choices=MONTH)
    #account_file_path = serializers.CharField(max_length=500)
    #navitaire_file_path = serializers.CharField(max_length=500)