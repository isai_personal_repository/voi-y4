from django.db import models
from django.contrib.auth.models import User


class Catalog(models.Model):
    name = models.CharField(max_length=50) # Ej. Desc_ing_pax_0221
    created = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey('Company', on_delete=models.DO_NOTHING) # Y4 / Q6
    total_amount = models.CharField(max_length=19)
    account = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.name.capitalize() + ' - ' + '{}'.format(self.created)

class Company(models.Model):
    name = models.CharField(max_length=10)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name.capitalize() + ' - ' + '{}'.format(self.created)

class Skyledger(models.Model):
    """Class to allocate all skyledger files"""
    account_period = models.CharField(max_length=25, blank=True, null=True)
    proper_accoount_period = models.CharField(max_length=25, blank=True, null=True)
    credit_account = models.CharField(max_length=15, blank=True, null=True)
    credit_center = models.CharField(max_length=15, blank=True, null=True)
    debit_account = models.CharField(max_length=15, blank=True, null=True)
    debit_center = models.CharField(max_length=15, blank=True, null=True)
    account_event = models.CharField(max_length=3, blank=True, null=True)
    account_type = models.CharField(max_length=2, blank=True, null=True)
    account_mapping_id = models.CharField(max_length=5, blank=True, null=True)
    journal_entry = models.CharField(max_length=6, blank=True, null=True)
    host_amount = models.DecimalField(max_digits=8, decimal_places=4, blank=True, null=True)
    host_currency = models.CharField(max_length=3, blank=True, null=True)
    local_amount = models.DecimalField(max_digits=8, decimal_places=4, blank=True, null=True)
    local_currency = models.CharField(max_length=3, blank=True, null=True)
    posting_date = models.CharField(max_length=25, blank=True, null=True)
    reference_date = models.CharField(max_length=25, blank=True, null=True)
    reference_code = models.CharField(max_length=10, blank=True, null=True)
    transaction_key = models.CharField(max_length=6, blank=True, null=True)
    ledger_version = models.IntegerField(default=0, blank=True, null=True)
    is_revenue = models.BooleanField(default=1)
    suspense_type_id = models.IntegerField(default=0, blank=True, null=True)

    record_created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    def __str__(self):
        return 