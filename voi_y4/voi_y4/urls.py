"""voi_y4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.apps import apps

skyledger = apps.get_app_config('skyledger').verbose_name
zendesk = apps.get_app_config('zendesk').verbose_name
user = apps.get_app_config('user').verbose_name

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('skyledger/', include(('apps.skyledger.api.v1.urls', skyledger), namespace='skyledger-api-v1')),
    path('zendesk/', include(('apps.zendesk.api.v1.urls', zendesk), namespace='zendesk-api-v1')),
    path('user/', include(('apps.user.api.v1.urls', user), namespace='user-api-v1')),

    path('api-auth/', include('rest_framework.urls'))
] # + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
